# LANGUAGE translation of Drupal (general)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  updates.inc,v 1.220 2006/03/26 14:06:06 killes
#  common.inc,v 1.529 2006/03/28 09:29:23 killes
#  image.inc,v 1.12 2005/12/14 20:06:41 dries
#  unicode.inc,v 1.17 2006/03/13 21:44:49 unconed
#  legacy.module,v 1.8 2006/02/21 18:46:54 dries
#  system.module,v 1.301 2006/03/31 04:57:49 drumm
#  aggregator.module,v 1.275 2006/03/08 09:52:13 dries
#  blogapi.module,v 1.80 2006/03/30 21:30:25 killes
#  book.module,v 1.359 2006/03/27 18:02:48 killes
#  comment.module,v 1.446 2006/03/30 17:23:29 unconed
#  contact.module,v 1.47 2006/03/23 09:29:35 killes
#  filter.module,v 1.113 2006/03/14 15:15:46 unconed
#  menu.module,v 1.70 2006/03/23 21:48:36 killes
#  node.module,v 1.624 2006/03/30 20:23:39 unconed
#  profile.module,v 1.146 2006/03/26 19:31:00 killes
#  user.module,v 1.602 2006/03/28 12:40:13 killes
#  form.inc,v 1.87 2006/03/29 23:29:41 drumm
#  locale.inc,v 1.69 2006/03/17 18:35:56 killes
#  block.module,v 1.203 2006/03/04 17:54:58 dries
#  path.module,v 1.81 2006/03/08 15:43:36 dries
#  drupal.module,v 1.118 2006/02/28 21:32:51 dries
#  statistics.module,v 1.222 2006/03/26 19:31:00 killes
#  upload.module,v 1.86 2006/03/30 17:45:32 killes
#  forum.module,v 1.323 2006/03/31 06:43:46 killes
#  taxonomy.module,v 1.268 2006/03/26 16:59:17 killes
#  watchdog.module,v 1.142 2006/03/27 07:29:34 unconed
#  search.module,v 1.170 2006/03/31 04:57:49 drumm
#  locale.module,v 1.135 2006/03/17 18:35:56 killes
#  menu.inc,v 1.112 2006/03/20 20:48:19 killes
#  chameleon.theme,v 1.42 2006/03/07 11:28:22 dries
#  pager.inc,v 1.53 2006/01/15 16:55:35 dries
#  theme.inc,v 1.286 2006/03/28 09:29:23 killes
#  phptemplate.engine,v 1.30 2006/03/13 21:42:35 unconed
#  xmlrpc.inc,v 1.31 2005/12/27 18:42:49 unconed
#  xmlrpcs.inc,v 1.18 2005/12/10 19:26:47 dries
#  blog.module,v 1.245 2006/03/27 18:02:48 killes
#  page.module,v 1.154 2006/03/27 18:02:48 killes
#  story.module,v 1.186 2006/03/27 18:02:48 killes
#  tracker.module,v 1.128 2006/03/17 18:56:25 killes
#  archive.module,v 1.87 2006/03/07 11:36:49 dries
#  throttle.module,v 1.59 2006/02/21 18:46:54 dries
#  help.module,v 1.50 2006/03/12 01:56:11 unconed
#  poll.module,v 1.190 2006/03/26 19:31:00 killes
#  cron.php,v 1.34 2005/12/31 14:18:22 dries
#  database.pgsql.inc,v 1.27 2006/02/26 19:48:43 dries
#  tablesort.inc,v 1.37 2005/12/10 19:26:47 dries
#  ping.module,v 1.34 2006/02/21 18:46:54 dries
#
msgid ""
msgstr ""
"Project-Id-Version: Drupal 4.7.0\n"
"POT-Creation-Date: 2007-01-30 12:39+0100\n"
"PO-Revision-Date: 2007-04-20 21:15+0200\n"
"Last-Translator: Pekka L J Jalkanen <plj@iki.fi>\n"
"Language-Team: FINNISH <jaro.larnos@pp.inet.fi>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-Language: Finnish\n"
"X-Poedit-Country: FINLAND\n"

#: modules/tracker/tracker.module:15
msgid "The tracker module displays the most recently added or updated content to the website allowing users to see the most recent contributions. The tracker module provides user level tracking for those who like to follow the contributions of particular authors."
msgstr "Seurantamoduuli näyttää uusimman sivustolle luodun ja viimeisimmäksi päivitetyn sisällön seurannan, antaen tällä tavoin käyttäjille mahdollisuuden tarkkailla sivuston päivittymistä. Moduuli mahdollistaa myös käyttäjäkohtaisen seurannan niitä käyttäjiä silmälläpitäen, jotka haluavat pysyä ajan tasalla tiettyjen käyttäjien luoman sisällön osalta."

#: modules/tracker/tracker.module:16
msgid "The  &quot;recent posts&quot; page is available via a link in the navigation menu block and contains a reverse chronological list of new and recently-updated content. The table displays  the content type, the title, the author's name, how many comments that item has received, and when it was last updated. Updates include any changes to the text, either by the original author or someone else, as well as any new comments added to an item. To use the tracker module to <em>watch</em> for a user's updated content, click on that user's profile, then the <em>track</em> tab."
msgstr "Navigaatiolohkon linkin kautta saatavilla olevalla &quot;Uusimmat kirjoitukset&quot; -sivulla on käänteisessä aikajärjestyksessä oleva lista uudesta ja hiljattain päivitetystä sisällöstä. Taulukko näyttää sisällön tyypin, otsikon, kirjoittajan nimen, tiedon siitä, kuinka monta kertaa kirjoitusta on kommentoitu, ja kirjoituksen viimeisimmän päivitysajankohdan. Päivitykseksi huomioidaan mikä tahansa muutos kirjoituksen tekstiin kenen hyvänsä käyttäjän toimesta, kuten myös kirjoituksen kommentointi. Seurantamoduulia voidaan käyttää myös yksittäisen käyttäjän päivitetyn sisällön tarkasteluun siirtymällä käyttäjän käyttäjäsivulle ja klikkaamalla välilehteä <em>seuraa</em>."

#: modules/tracker/tracker.module:17
msgid "For more information please read the configuration and customization handbook <a href=\"@tracker\">Tracker page</a>."
msgstr "Lisätietoa on saatavissa asetukset ja muokkaus -käsikirjan <a href=\"%tracker\">Tracker-sivulla</a>."

#: modules/tracker/tracker.module:30
msgid "Recent posts"
msgstr "Uusimmat kirjoitukset"

#: modules/tracker/tracker.module:35
msgid "All recent posts"
msgstr "Kaikki uusimmat kirjoitukset"

#: modules/tracker/tracker.module:37
msgid "My recent posts"
msgstr "Viimeisimmät kirjoitukseni"

#: modules/tracker/tracker.module:46
msgid "Track posts"
msgstr "Seuraa kirjoittelua"

#: modules/tracker/tracker.module:113
msgid "!time ago"
msgstr "!time sitten"

#: modules/tracker/tracker.module:121
msgid "Post"
msgstr "Kirjoitus"

#: modules/tracker/tracker.module:121
msgid "Last updated"
msgstr "Viimeksi päivitetty"

#: modules/tracker/tracker.module:0
msgid "tracker"
msgstr "seuranta"

